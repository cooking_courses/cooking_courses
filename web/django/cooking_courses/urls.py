# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from rest_framework import routers

from cooking_courses.views import CookingCourseView, CourseRegistrationView

router = routers.DefaultRouter()  # pylint: disable=invalid-name
router.register(r'course', CookingCourseView, 'cooking_course')
router.register(r'registration', CourseRegistrationView, 'course_registration')

urlpatterns = [  # pylint: disable=invalid-name
    url(r'^', include(router.urls, namespace='cooking_courses'))
]
