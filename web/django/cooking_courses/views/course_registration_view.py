# -*- coding: utf-8 -*-
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, mixins, permissions, viewsets

from cooking_courses.models import CourseRegistration
from cooking_courses.serializers import CourseRegistrationSerializer


class CourseRegistrationView(mixins.CreateModelMixin, viewsets.GenericViewSet):  # pylint: disable=too-many-ancestors
    serializer_class = CourseRegistrationSerializer
    queryset = CourseRegistration.objects.all()
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    permission_classes = (permissions.AllowAny,)
