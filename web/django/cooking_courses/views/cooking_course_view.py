# -*- coding: utf-8 -*-
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, permissions, viewsets
from cooking_courses.models import CookingCourse
from cooking_courses.serializers import CookingCourseSerializer


class CookingCourseView(viewsets.ModelViewSet):  # pylint: disable=too-many-ancestors
    serializer_class = CookingCourseSerializer
    queryset = CookingCourse.objects.all()
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly,)
