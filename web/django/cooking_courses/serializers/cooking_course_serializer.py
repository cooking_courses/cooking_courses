# -*- coding: utf-8 -*-
from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from cooking_courses.models import CookingCourse


class CookingCourseSerializer(serializers.ModelSerializer):
    teacher = serializers.PrimaryKeyRelatedField(queryset=get_user_model().objects.all(), required=False)
    teacher_email = serializers.EmailField(source='teacher.email', read_only=True)

    class Meta:
        model = CookingCourse
        fields = (
            tuple(field.name for field in CookingCourse._meta.get_fields() if field not in ('registrations',)) +
            ('teacher_email',)
        )

    def validate(self, data):
        validated_data = super().validate(data)
        start = validated_data.get('start', None)
        end = validated_data.get('end', None)
        if start is not None and end is not None and start > end:
            raise ValidationError('end date (%s) should occur after start date (%s)' % (end, start))
        return validated_data
