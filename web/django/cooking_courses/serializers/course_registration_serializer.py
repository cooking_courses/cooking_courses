# -*- coding: utf-8 -*-
from django_countries.serializer_fields import CountryField
from rest_framework import serializers

from cooking_courses.models import CourseRegistration


class CourseRegistrationSerializer(serializers.ModelSerializer):
    country = CountryField()

    class Meta:
        model = CourseRegistration
        fields = tuple(field.name for field in CourseRegistration._meta.get_fields())
