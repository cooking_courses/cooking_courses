from django.apps import AppConfig


class CookingCoursesConfig(AppConfig):
    name = 'cooking_courses'
