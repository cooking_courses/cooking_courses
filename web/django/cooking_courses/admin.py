# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import CookingCourse, CourseRegistration


admin.site.register(CookingCourse)
admin.site.register(CourseRegistration)
