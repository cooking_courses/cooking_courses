# -*- coding: utf-8 -*-
# pylint: disable=redefined-outer-name
import pytest
from django.contrib.auth.models import Group, Permission
from pytest_factoryboy import register

from cooking_courses.models import create_teachers_group
from users.tests.factories.models import UserFactory
from users.tests.utils import get_annon_client, get_jwt_client

from .factories import CookingCourseFactory, CourseRegistrationFactory

register(CookingCourseFactory)
register(CourseRegistrationFactory)
register(UserFactory)


@pytest.fixture()
def user():
    return UserFactory.create(username='test_user')  # pylint: disable=no-member


@pytest.fixture()
def teacher():
    create_teachers_group(Permission, Group)
    teacher = UserFactory.create(username='test_teacher')  # pylint: disable=no-member
    teachers = Group.objects.get(name='Teachers')
    teacher.groups.set([teachers])
    teacher.save()
    return teacher


@pytest.fixture()
def user_client(user):
    return get_jwt_client(user)


@pytest.fixture()
def annon_client():
    return get_annon_client()


@pytest.fixture()
def teacher_client(teacher):
    return get_jwt_client(teacher)
