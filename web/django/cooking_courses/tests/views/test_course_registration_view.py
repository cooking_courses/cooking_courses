# -*- coding: utf-8 -*-
import pytest
from django.urls import reverse

from users.tests.utils import check_response
from cooking_courses.serializers import CourseRegistrationSerializer


@pytest.mark.django_db(transaction=True)
def test_registration_list_ko(annon_client, course_registration_factory):
    course_registration_factory.create()
    course_registration_factory.create()
    course_registrations_url = reverse('cooking_courses:course_registration-list')

    response = annon_client.get(course_registrations_url)

    check_response(response, 405)


@pytest.mark.django_db(transaction=True)
def test_course_registration_post(annon_client, cooking_course_factory, course_registration_factory):
    course = cooking_course_factory.create()
    course_registration = course_registration_factory.build(
        course=course
    )
    serializer = CourseRegistrationSerializer(course_registration)
    data = serializer.data

    course_registration_url = reverse('cooking_courses:course_registration-list')
    response = annon_client.post(course_registration_url, data)

    check_response(response, 201)
