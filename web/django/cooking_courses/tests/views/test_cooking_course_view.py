# -*- coding: utf-8 -*-
import pytest
from django.urls import reverse

from users.tests.utils import check_response
from cooking_courses.serializers import CookingCourseSerializer


@pytest.mark.django_db(transaction=True)
def test_cooking_course_list(annon_client, cooking_course_factory):
    cooking_course_factory.create()
    cooking_course_factory.create()
    cooking_courses_url = reverse('cooking_courses:cooking_course-list')

    response = annon_client.get(cooking_courses_url)

    data = check_response(response, 200)
    assert len(data) == 2


@pytest.mark.django_db(transaction=True)
def test_cooking_course_get(annon_client, cooking_course_factory):
    cooking_course = cooking_course_factory.create()
    cooking_course_url = reverse('cooking_courses:cooking_course-detail', args=(cooking_course.id,))

    response = annon_client.get(cooking_course_url)

    data = check_response(response, 200)
    assert data['id'] == cooking_course.id
    assert data['teacher_email'] is not None


@pytest.mark.django_db(transaction=True)
def test_cooking_course_delete_forbidden(annon_client, cooking_course_factory):
    cooking_course = cooking_course_factory.create()
    cooking_course_url = reverse('cooking_courses:cooking_course-detail', args=(cooking_course.id,))
    response = annon_client.delete(cooking_course_url)

    check_response(response, 401)


@pytest.mark.django_db(transaction=True)
def test_cooking_course_update_forbidden(annon_client, cooking_course_factory):
    cooking_course = cooking_course_factory.create()
    cooking_course_url = reverse('cooking_courses:cooking_course-detail', args=(cooking_course.id,))

    response = annon_client.get(cooking_course_url)
    data = check_response(response, 200)

    response = annon_client.post(cooking_course_url, data)
    check_response(response, 401)


@pytest.mark.django_db(transaction=True)
def test_cooking_course_delete(teacher_client, cooking_course_factory):
    cooking_course = cooking_course_factory.create()
    cooking_course_url = reverse('cooking_courses:cooking_course-detail', args=(cooking_course.id,))
    response = teacher_client.delete(cooking_course_url)

    check_response(response, 204)


@pytest.mark.django_db(transaction=True)
def test_cooking_course_post(teacher, teacher_client, cooking_course_factory):
    cooking_course = cooking_course_factory.build(
        teacher=teacher
    )
    serializer = CookingCourseSerializer(cooking_course)
    data = serializer.data

    cooking_course_url = reverse('cooking_courses:cooking_course-list')
    response = teacher_client.post(cooking_course_url, data)

    check_response(response, 201)


@pytest.mark.django_db(transaction=True)
def test_cooking_course_put(teacher, teacher_client, cooking_course_factory):
    cooking_course = cooking_course_factory.create(
        teacher=teacher
    )
    serializer = CookingCourseSerializer(cooking_course)
    data = serializer.data
    data['name'] = 'new great name'

    cooking_course_url = reverse('cooking_courses:cooking_course-detail', args=(cooking_course.id,))
    response = teacher_client.put(cooking_course_url, data)

    data = check_response(response, 200)
    assert data['name'] == 'new great name'


@pytest.mark.django_db(transaction=True)
def test_cooking_course_post_invalid_dates(teacher, teacher_client, cooking_course_factory):
    cooking_course = cooking_course_factory.build(
        teacher=teacher
    )
    serializer = CookingCourseSerializer(cooking_course)
    data = serializer.data
    start = data['start']
    end = data['end']
    data['start'] = end
    data['end'] = start

    cooking_course_url = reverse('cooking_courses:cooking_course-list')
    response = teacher_client.post(cooking_course_url, data)

    check_response(response, 400)
