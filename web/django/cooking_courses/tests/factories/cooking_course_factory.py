# -*- coding: utf-8 -*-
import factory
from django.utils import timezone
from faker import Factory as FakerFactory
from datetime import timedelta

from cooking_courses.models import CookingCourse
from users.tests.factories.models import UserFactory

faker = FakerFactory.create()


class CookingCourseFactory(factory.django.DjangoModelFactory):
    name = factory.LazyAttribute(lambda x: ' '.join(faker.words(nb=3)))
    description = factory.LazyAttribute(lambda x: ' '.join(faker.sentences(nb=2)))
    teacher = factory.SubFactory(UserFactory)
    registration_open = True
    start = factory.LazyAttribute(
        lambda x: faker.date_time_between(start_date='now', end_date='+5d', tzinfo=timezone.utc)
    )
    end = factory.LazyAttribute(
        lambda x: x.start + timedelta(days=15)
    )

    class Meta:
        model = CookingCourse
