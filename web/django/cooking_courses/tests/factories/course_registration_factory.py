# -*- coding: utf-8 -*-
import factory
from faker import Factory as FakerFactory

from cooking_courses.models import CourseRegistration

from . import CookingCourseFactory

faker = FakerFactory.create()


class CourseRegistrationFactory(factory.django.DjangoModelFactory):
    first_name = factory.LazyAttribute(lambda x: faker.first_name())
    last_name = factory.LazyAttribute(lambda x: faker.last_name())
    country = factory.LazyAttribute(lambda x: faker.country_code())
    zip_code = factory.LazyAttribute(lambda x: faker.zipcode())
    course = factory.SubFactory(CookingCourseFactory)

    class Meta:
        model = CourseRegistration
