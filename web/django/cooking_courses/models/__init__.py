# -*- coding: utf-8 -*-
from .cooking_course import CookingCourse, create_teachers_group
from .course_registration import CourseRegistration
