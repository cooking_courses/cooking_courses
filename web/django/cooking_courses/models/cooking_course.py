# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.db import models
from model_utils.models import TimeFramedModel


class CookingCourse(TimeFramedModel):
    name = models.CharField(max_length=200, blank=False, null=False)
    description = models.TextField(blank=False, null=True)
    teacher = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    registration_open = models.BooleanField(default=True)


def create_teachers_group(Permission, Group):  # pylint: disable=invalid-name
    content_type = ContentType.objects.get_for_model(CookingCourse)
    delete_perm, _ = Permission.objects.get_or_create(
        codename='delete_cookingcourse',
        content_type=content_type,
        name='Can delete cooking course'
    )
    add_perm, _ = Permission.objects.get_or_create(
        codename='add_cookingcourse',
        content_type=content_type,
        name='Can add cooking course'
    )
    change_perm, _ = Permission.objects.get_or_create(
        codename='change_cookingcourse',
        content_type=content_type,
        name='Can change cooking course'
    )
    teachers, _ = Group.objects.get_or_create(name='Teachers')
    teachers.permissions.set([delete_perm, add_perm, change_perm])
    teachers.save()
