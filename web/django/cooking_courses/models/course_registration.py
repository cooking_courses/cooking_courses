# -*- coding: utf-8 -*-
from django.db import models
from django_countries.fields import CountryField

from . import CookingCourse


class CourseRegistration(models.Model):
    first_name = models.CharField(max_length=50, blank=False, null=False)
    last_name = models.CharField(max_length=50, blank=True, null=True)
    country = CountryField(blank=False, null=False)
    zip_code = models.CharField(max_length=30, blank=False, null=False)
    course = models.ForeignKey(CookingCourse, on_delete=models.CASCADE, related_name='registrations')
