# -*- coding: utf-8 -*-
# pylint: disable=invalid-name
from __future__ import unicode_literals

from django.db import migrations
from cooking_courses.models import create_teachers_group as create_group


def create_teachers_group(apps, schema_editor):  # pylint: disable=unused-argument
    Group = apps.get_model('auth', 'Group')  # pylint: disable=invalid-name
    Permission = apps.get_model('auth', 'Permission')  # pylint: disable=invalid-name
    create_group(Permission, Group)


class Migration(migrations.Migration):

    dependencies = [
        ('cooking_courses', '0003_auto_20170331_0750'),
    ]

    operations = [
        migrations.RunPython(create_teachers_group),
    ]
