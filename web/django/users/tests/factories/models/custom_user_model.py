# -*- coding: utf-8 -*-
from django.contrib.auth import get_user_model
import factory
from faker import Factory as FakerFactory


faker = FakerFactory.create()


class UserFactory(factory.django.DjangoModelFactory):

    username = factory.Sequence(lambda n: "user_%d" % n)
    email = factory.LazyAttribute(lambda x: "%s@mail.com" % x.username)
    password = factory.LazyAttribute(lambda x: x.username)
    first_name = factory.LazyAttribute(lambda x: faker.first_name())
    last_name = factory.LazyAttribute(lambda x: faker.last_name())

    class Meta:
        model = get_user_model()
