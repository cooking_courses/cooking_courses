# -*- coding: utf-8 -*-
import pytest
from django.urls import reverse

from users.tests.utils import check_response


@pytest.mark.django_db(transaction=True)
def test_registration(admin_client, client):
    registration_url = reverse('registration')
    token_auth_url = reverse('token-auth')
    credentials = {'email': 'test_user@user.com', 'password': 'pass1234'}
    test_user_data = dict(credentials)
    test_user_data.update({'first_name': 'User', 'last_name': 'Last'})
    response = admin_client.post(registration_url, data=test_user_data)
    data = check_response(response, 201)
    assert 'token' in data
    assert data['user']['first_name'] == 'User'
    response = client.post(token_auth_url, data=credentials)
    data = check_response(response, 200)
    assert 'token' in data
