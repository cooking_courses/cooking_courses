# -*- coding: utf-8 -*-
import json

import pytest
from django.test.client import Client
from rest_framework_jwt.settings import api_settings


def check_response(response, code):
    """
    Helper function to test if the response.status_code is the expected.
    """
    if response.status_code != code:
        data = pretty_print_data(response)
        pytest.fail(
            'Unexpected status code: %d != %d ==> %s' % (response.status_code, code, data)
        )
    return get_response_data(response)


def pretty_print_data(response):
    if response.data:
        if 'application/json' in response.get('Content-Type', None):
            return json.dumps(response.json(), indent=4, sort_keys=True)
        else:
            return response.data
    return '<NO DATA>'


def get_response_data(response):
    if response.data:
        if 'application/json' in response.get('Content-Type', None):
            return response.json()
    return None


def get_token(user):
    """
    Uses JWT configuration to obtain a valid token for the given user.
    """
    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

    payload = jwt_payload_handler(user)
    return jwt_encode_handler(payload)


class JSONClient(Client):
    """
    Override default client to use JSON
    """
    # pylint: disable=too-many-arguments
    JSON_CONTENT_TYPE = 'application/json'

    def post(self, path, data=None, content_type=None, follow=False, secure=False, **extra):
        if data and not isinstance(data, str):
            data = json.dumps(data)
        return super().post(
            path, data, content_type=(content_type or self.JSON_CONTENT_TYPE),
            follow=follow, secure=secure, **extra
        )

    def put(self, path, data=None, content_type=None, follow=False, secure=False, **extra):
        if data and not isinstance(data, str):
            data = json.dumps(data)
        return super().put(
            path, data, content_type=(content_type or self.JSON_CONTENT_TYPE),
            follow=follow, secure=secure, **extra
        )

    def patch(self, path, data=None, content_type=None, follow=False, secure=False, **extra):
        if data and not isinstance(data, str):
            data = json.dumps(data)
        return super().patch(
            path, data, content_type=(content_type or self.JSON_CONTENT_TYPE),
            follow=follow, secure=secure, **extra
        )

    def delete(self, path, data=None, content_type=None, follow=False, secure=False, **extra):
        if data and not isinstance(data, str):
            data = json.dumps(data)
        return super().delete(
            path, data, content_type=(content_type or self.JSON_CONTENT_TYPE),
            follow=follow, secure=secure, **extra
        )


def get_jwt_client(user):
    """
    Given an user returns a django.test.client that is ready to perform JSON queries
    and is authenticated using JWT
    """
    token = get_token(user)
    jwt_prefix = api_settings.JWT_AUTH_HEADER_PREFIX
    headers = {
        'Content-Type': 'application/json',
        'HTTP_AUTHORIZATION': '%s %s' % (jwt_prefix, token)
    }
    return JSONClient(enforce_csrf_checks=True, **headers)


def get_annon_client():
    return JSONClient(enforce_csrf_checks=True)
