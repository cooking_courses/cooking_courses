# -*- coding: utf-8 -*-
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver


class CustomUserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, fusername=None, femail=None, fpassword=None, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        email = extra_fields.pop('email', None)
        username = extra_fields.pop('username', None)
        femail = femail or email
        fusername = fusername or username or femail
        if not femail:
            raise ValueError('The given email must be set')
        if not fusername:
            raise ValueError('The given username must be set')
        femail = self.normalize_email(femail)
        fusername = self.model.normalize_username(fusername)
        user = self.model(username=fusername, email=femail, **extra_fields)
        user.set_password(fpassword)
        user.save(using=self._db)
        return user

    def create_user(self, fusername=None, femail=None, fpassword=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(fusername, femail, fpassword, **extra_fields)

    def create_superuser(self, fusername=None, femail=None, fpassword=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(fusername, femail, fpassword, **extra_fields)


class CustomUserModel(AbstractUser):
    """
    Custom model that simply sets that the field to identify the user should be the email instead of
    the username
    """
    email = models.EmailField(verbose_name='email address', max_length=255, unique=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ('username',)

    objects = CustomUserManager()


@receiver(pre_save, sender=CustomUserModel)
def set_username(sender, **kwargs):  # pylint: disable=unused-argument
    user = kwargs["instance"]
    user.username = user.email
