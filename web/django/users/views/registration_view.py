# -*- coding: utf-8 -*-
from rest_auth.registration.views import RegisterView
from rest_framework.permissions import IsAdminUser

from users.serializers import RegistrationSerializer


class RegistrationView(RegisterView):
    permission_classes = (IsAdminUser, )

    """
    Registers a new user.
    """
    serializer_class = RegistrationSerializer
