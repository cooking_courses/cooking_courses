# -*- coding: utf-8 -*-
from django.contrib.auth import get_user_model
from rest_framework import viewsets
from rest_framework import mixins

from users.serializers import UserSerializer


class UserViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    """
    This viewset allows to get user info via GET, no listing, deleting or creation is allowed.
    """
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
