# -*- coding: utf-8 -*-
from rest_auth.registration.views import VerifyEmailView


class EmailConfirmationView(VerifyEmailView):
    """
    Confirms that the user email is correct using a token send on the confirmation email.
    """
    pass
