# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token

from users.views import EmailConfirmationView, RegistrationView, UserViewSet

router = routers.DefaultRouter()  # pylint: disable=invalid-name
router.register(r'accounts', UserViewSet)

urlpatterns = [  # pylint: disable=invalid-name
    url(r'^', include(router.urls)),
    url(r'^registration/$', RegistrationView.as_view(), name='registration'),
    url(r'^confirm-email/$', EmailConfirmationView.as_view(), name='confirm_email'),
    # This entry point is used on a reverse() call during the email confirmation generation
    url(r'^confirm-email/(?P<key>[-:\w]+)/$', EmailConfirmationView.as_view(), name='account_confirm_email'),
    url(r'^auth/token-auth/', obtain_jwt_token, name='token-auth'),
    url(r'^auth/token-refresh/', refresh_jwt_token, name='token-refresh'),
    url(r'^auth/token-verify/', verify_jwt_token, name='token-verify')
]
