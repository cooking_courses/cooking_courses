from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model


class Command(BaseCommand):
    help = "Creates a new user."

    def add_arguments(self, parser):
        parser.add_argument('--super', action='store_true', default=False, help='The user will be a superuser')
        parser.add_argument('--staff', action='store_true', default=False, help='The user will be staff')
        parser.add_argument('username', nargs=1)
        parser.add_argument('email', nargs=1)
        parser.add_argument('password', nargs=1)

    def handle(self, *args, **options):

        extra = {}
        if options['super']:
            extra['is_staff'] = True
            extra['is_superuser'] = True
        if options['staff']:
            extra['is_staff'] = True

        get_user_model().objects.create_user(
            options['username'][0], options['email'][0], options['password'][0], **extra
        )
