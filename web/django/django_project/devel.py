# -*- coding: utf-8 -*-

from .base import *  # NOQA

DEBUG = True

# PROJECT_DIR = os.path.abspath(os.path.dirname(__file__))
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(PROJECT_DIR, 'yourdatabasename.db'),
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'store-devel',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'db-devel',
        'PORT': 5432
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

# Adding the admin site and extensions to make development easier
INSTALLED_APPS = INSTALLED_APPS + (
    'django_extensions',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler'
        }
    },
    'loggers': {
        'werkzeug': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True
        }
    }
}


# Adding default authentications to login using normal sessions
REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES'].extend([
    'rest_framework.authentication.SessionAuthentication',
    'rest_framework.authentication.BasicAuthentication'
    ])

# do not send emails only print them on the console
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
