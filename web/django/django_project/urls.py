# -*- coding: utf-8 -*-
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [  # pylint: disable=c0103
    url(r'^api/users/', include('users.urls')),
    # adds grappelli improvements to default admin site
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
    # REST framework auth for the browsable API
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'api/cooking_courses/', include('cooking_courses.urls'))
]

if settings.DEBUG:
    urlpatterns.extend([])
