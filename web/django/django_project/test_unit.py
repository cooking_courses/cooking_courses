# -*- coding: utf-8 -*-

from .base import *  # NOQA

DEBUG = True
FAKER_SETTINGS_LOCALE = "en_GB"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'memory:',
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

INSTALLED_APPS = INSTALLED_APPS + (
    'django_extensions',
)

REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES'].extend([
    'rest_framework.authentication.SessionAuthentication',
    'rest_framework.authentication.BasicAuthentication'
    ])

# do not send emails only print them on the console
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
