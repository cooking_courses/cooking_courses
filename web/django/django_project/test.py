# -*- coding: utf-8 -*-

from .base import *  # NOQA

FAKER_SETTINGS_LOCALE = "en_GB"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'store-test',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'db-test',
        'PORT': 5432
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

STATIC_ROOT = '/static'
ALLOWED_HOSTS = ['web', 'localhost', '127.0.0.1', '[::1]']
X_FRAME_OPTIONS = 'DENY'
CSRF_COOKIE_SECURE = True
CSRF_COOKIE_HTTPONLY = True
SESSION_COOKIE_SECURE = True
SECURE_SSL_REDIRECT = True
SECURE_BROWSER_XSS_FILTER = True
SECORE_CONTENT_TYPE_NOSNIFF = True
SECURE_HSTS_SECONDS = 31536000
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_HSTS_INCLUDE_SUBDOMAINS = True

# do not send emails only print them on the console
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
