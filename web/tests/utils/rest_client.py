# -*- coding: utf-8 -*-
import json

import requests


class UnexpectedHTTPStatusError(Exception):

    def __init__(self, response, expected_status_code):
        msg = 'Expected: %d Found: %d Body: %s' % (expected_status_code, response.status_code, response.json())
        super().__init__(msg)


class RESTClient(object):
    """
    Make easy the task of testing the REST API
    """
    headers = {
        'Content-Type': 'application/json'
    }

    def __init__(self, base_url, verify_ssl=True):
        self.verify_ssl = verify_ssl
        self.base_url = base_url

    def post(self, url, data, expected_status_code=201):
        response = requests.post(
            "%s%s" % (self.base_url, url),
            data=json.dumps(data),
            verify=self.verify_ssl,
            headers=self.headers
        )
        if response.status_code != expected_status_code:
            raise UnexpectedHTTPStatusError(response, expected_status_code)
        return response.json()
