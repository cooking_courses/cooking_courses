# -*- coding: utf-8 -*-
import os

import pytest

from .utils import RESTClient


@pytest.fixture(scope='session')
def rest_client():
    base_url = os.environ.get('PYTEST_BASE_URL', None)
    if not base_url:
        raise RuntimeError('You must set the environment var PYTEST_BASE_URL')
    verify_ssl = bool(os.environ.get('PYTEST_VERIFY_SSL', True))
    base_url = base_url.rstrip('/')
    return RESTClient(base_url, verify_ssl)
