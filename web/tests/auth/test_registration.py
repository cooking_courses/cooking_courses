# -*- coding: utf-8 -*-


def test_registration(rest_client):
    credentials = {'email': 'test_user@user.com', 'password': 'pass1234'}
    test_user_data = dict(credentials)
    test_user_data.update({'first_name': 'User', 'last_name': 'Last'})
    response = rest_client.post('/api/users/registration/', data=test_user_data)
    assert 'token' in response
    assert response['user']['first_name'] == 'User'
    response = rest_client.post('/api/users/auth/token_auth/', data=credentials)
    assert 'token' in response
