find ./web/django | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
docker build web -f ./web/Dockerfile-prod -t ogarcia/web-prod:3.6
