find ./web/django | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
docker build web-env -f ./web-env/Dockerfile-prod -t ogarcia/web-env-prod:3.6
