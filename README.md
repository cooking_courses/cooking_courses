# README #

Django basic project working with restframework to provide a basic REST API to manage users.
This project is ready to use with Docker.


## Installation ##
There are three different environments used to work with the codebase in different scenarios:

* devel: Using for development purposes, Django is configured to be verbose and easy to debug.
* prod: Used to deploy in production mode. Django is configured with security and responsiveness in mind.
* test (experimental): Used to emulate production environment and run integration tests.
* pytest (experimental): Used to run unit tests inside a Docker container.

Each configuration or script file is named according to the environment is meant to be use.

In the scripts folder there are some scripts to build those images. Images are build on top of each other
so it is faster to build them every time a change is made. In production maybe it is more
efficient to build one single image, there is a Docker command to do that.

### Development ###
To start working with the project in development mode using Docker containers first it is needed to build
the images.

1. `./scripts/build-python-alpine.sh`: Will create a Linux Alpine image that can run Python programs. This
image will also prepare the support to use PostgreSQL and will add Bash to be able to access the image
using a console.
2. `./scripts/build-web-env-base.sh`: Will create a image on top of python-alpine installing the base dependencies
to run Django. This dependencies are common to all the environments.
3. `./scripts/build-web-env-devel.sh`: Will create a image on top of web-env-base installing the dependencies to
run Django in development mode, adding some tools to debug. In the web-env/requirements folder there are the list
of packages being installed.
4. `./scripts/build-web-devel.sh` : Will create a devel environment image and copy all source and configuration files.
5. There is a docker-comose-devel.yml file That will use a PostgreSQL Docker image and run web-devel Django image
in development mode. The command `docker-compose -f docker-compose-devel.yml up` will boot the images and connect
the services. In development mode the image will use the local folder as volume, so changes in the code are
immediately reflected on the image.
6. If you navigate in your browser to <http://localhost:8000/admin/> you should be able to see Django Admin
interface.

### Production ####
To run Django in production mode the steps are similar to that of development.

1. `./scripts/build-python-alpine.sh`: Will create a Linux Alpine image that can run Python programs. This
image will also prepare the support to use PostgreSQL and will add Bash to be able to access the image
using a console.
2. `./scripts/build-web-env-base.sh`: Will create a image on top of python-alpine installing the base dependencies
to run Django. This dependencies are common to all the environments.
3. `./scripts/build-web-env-prod.sh`: Will create a image on top of web-env-base installing the dependencies to
run Django in production mode. In the web-env/requirements folder there are the list of installed packages.
4. `./scripts/build-web-prod.sh`: Will create a production  environment image and copy all source and configuration files.
5. `./scripts/build-nginx.sh`: Will create and configure a Nginx web server. Nginx will run UWSGI containing Django.
6. There is a docker-comose-prod.yml file That will use a PostgreSQL Docker image, memcache, NGinx,
and will run Django in production mode. The command `docker-compose -f docker-compose-prod.yml up` will boot the 
images and connect the services. You can use this image as reference if you run the services in different machines.
7. If you navigate in your browser to <http://localhost/admin/> you should be able to see Django Admin
interface. HTTPS protocol is also supported but the SSL credentials are self signed and so invalid.

## Run unit tests ##
Py.test is the tool used to run unit tests. Unit Tests can be run inside a Docker container (TODO) or in your local
environment. To run the tests locally follow the next steps.
1. Create a virtual environment: `mkvirtualenv -p python3 myenv`
2. Install the packages: `pip install -f ./web-env/requirements/local.txt`
3. Go to django dir: `cd ./web/django`
4. Set the Django settings: `export DJANGO_SETTINGS_MODULE=django_project.test_unit`
5. Run py.test and have fun!: `py.test -s`

## Cheat sheet ##

### Apply migrations ###
To apply migrations inside a docker container you should run a command similar to this one:
`docker-compose -f docker-compose-devel.yml run web-devel python manage.py migrate`

### Create Django superuser ###
To access the Django admin interface first you will need a user that has access privileges. You can create the
Django superuser (in development mode) running the next command:
`docker-compose -f docker-compose-devel.yml run web-devel python manage.py createsuperuser`


To create a regular user:
`docker-compose -f docker-compose-devel.yml run web-devel python manage.py create_user test_user test_user@mail.com test_user`

### Access PostgreSQL console ###
To access PostgreSQL console (in production mode): `docker-compose -f docker-compose-prod.yml run db psql -h db -U postgres`

Or run an standalone PostgreSQL container:
`docker run -it --rm --name db-devel -h db-devel -p 5432:5432 -v <FULL_PATH>/django-docker-barebones/pgdata-devel/:/var/lib/postgresql/data/ -e POSTGRES_PASSWORD=postgres -e POSTGRES_USER=postgres -e POSTGRES_DB=store-devel <IMAGE_ID>`

Now you should can access Postgres using PgAdmin for example on port 5432.

### Run bash inside a container ###
To run bash inside a running container (development mode): `docker-compose -f docker-compose-devel.yml run web-devel /bin/bash`

### Run Python shell inside a container ###
In development mode you can start a Python interactive session to debug your programs:
`docker-compose -f docker-compose-devel.yml run web-devel python manage.py shell_plus`
